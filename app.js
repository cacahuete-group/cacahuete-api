const fastify = require('fastify')({
  logger: false,
});
// require('./websocket');
const hostname = '0.0.0.0';
const port = process.env.PORT || 3002;

fastify.register(require('./routes/user.routes'));
fastify.register(require('./routes/station.routes'));
fastify.register(require('./routes/journey.routes'));
fastify.register(require('./routes/notification.routes'));
fastify.register(require('fastify-cors'), {
  origin: '*',
});
fastify.register(require('fastify-sensible'));

fastify.get('/', async (request, reply) => ({ hello: 'world' }));
// fastify.register(require("fastify-swagger"));

fastify.listen(port, hostname, (err) => {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});
