drop function prevent_admin_delete cascade
drop function prevent_double_user cascade;

create function prevent_admin_delete()
returns trigger
language plpgsql
as $$
begin
  if(old.type = 'admin')
  then
    raise exception 'Deletion failed, admin can not be deleted';
    rollback;
  else
    raise notice 'Deletion Succeed';
    return old;
  end if;
end; $$

create trigger on_delete_user
before delete on "Users"
for each row 
execute function prevent_admin_delete();


create function prevent_double_user()
returns trigger
language plpgsql
as $$
begin
  if exists(select * from "Users" where email ilike new.email or username ilike new.username)
  then
    raise exception 'User already exists';
    rollback;
  else
    raise notice 'Insertion Succeed';
    return new;
  end if;
end; $$

create trigger on_insert_user
before insert on "Users"
for each row 
execute function prevent_double_user();