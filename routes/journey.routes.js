const {
  createJourney,
  getFromUser,
  getJourneys,
  getJourneyById,
  getJourneyStation,
  searchJourneyAround,
} = require('../sequelize/controllers/journey.controller');

const {
  validateToken,
  destroy,
  getUserInfos,
} = require('../sequelize/controllers/token.controller');

const {
  createReservation,
} = require('../sequelize/controllers/reservation.controller');
const wss = require('../websocket');
const { notifyRegistrationJourney } = require('../sequelize/controllers/notification.controller');

module.exports = async (fastify, options) => {
  //Find journeys by paginating
  fastify.get('/journeys', async (request, reply) => {
    const page = request.query.page ?? 1;
    const per_page = request.query.per_page ?? 5;
    const journeys = await getJourneys(page, per_page);
    return journeys; //.toJSON();
  }),

    //Find all journeys for user
    fastify.get('/users/:user_id/journeys', async (request, reply) => {
      //sendMessageWS('get journeys');
      const token = request.headers.authorization;

      let response = await getUserInfos(token)
      const userId = request.params.user_id;

      if (response.id && response.username && response.email && response.type) {
        if (response.id !== userId && response.type !== USER.ADMIN_TYPE) {
          return reply.unauthorized("User don't have this right");
        }
        let journeys = await getFromUser(response.id);
        return journeys;
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    }),
    //Register user to a existing journey
    fastify.post(
      '/users/:userId/journeys/:journeyId',
      async (request, reply) => {
        const token = request.headers.authorization;

        let response = await getUserInfos(token);
        const journeyId = request.params.journeyId;
        const userId = request.params.userId;
        const journey = await getJourneyById(journeyId);
        //return await getJourneyStation(journeyId);
        if (
          response.id &&
          response.username &&
          response.email &&
          response.type
        ) {
          try {
            if (response.id !== userId)
              return reply.unauthorized(
                'Only the user itself can add a journey',
              );
            else if (!journey) return reply.notFound('Journey not found');
            //journeyI = journeyI;
            else if (journey) {
              const insert = await createReservation(userId, journeyId);
              notifyRegistrationJourney(journeyId, userId);
              if (insert) return { journey: insert };
              else
                return reply.conflict(
                  'User already registered for this journey',
                );
            }
          } catch (e) {
            return reply.notFound(e);
          }
        } else {
          if (response[1] === 'Token not found') reply.notFound(response[1]);
        }
      },
    ),
    //Create a new journey and add to a user
    fastify.post('/users/:userId/journeys', async (request, reply) => {
      const token = request.headers.authorization;
      const journey = request.body;

      let response = await getUserInfos(token);

      if (response.id && response.username && response.email && response.type) {
        try {
          let journeyI = await (await createJourney(
            journey,
            journey.departure_station,
            journey.arrival_station,
            response.id,
          )).toJSON();
          journeyI.userCount = 1;
          if (journeyI) return { journey: journeyI };
          else
            return reply.conflict('User already registered for this journey');
        } catch (e) {
          return reply.notFound(e);
        }
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    }),

    fastify.get('/journeys/departures', async (request, reply) => {
      let city = request.query.city;
      let postal_code = request.query.postal_code;

      return await searchJourneyAround(city, postal_code);
    });
};
