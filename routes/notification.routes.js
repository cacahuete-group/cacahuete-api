const { postNotifShema } = require('../sequelize/constants/notification.constants');
const {
  getNotifications, deleteNotifications, addNotification, notifyRegistrationJourney
} = require('../sequelize/controllers/notification.controller');
const { sendHttpErrors, getUserInfos } = require('../sequelize/controllers/token.controller');


module.exports = async (fastify, options) => {

  fastify.get('/notifications/:user_id', async (request, reply) => {

    const userId = request.params.user_id;
    const token = request.headers.authorization;

    const hasNoErrors = await sendHttpErrors(token, userId, reply);
    if (hasNoErrors) {
      return await getNotifications(userId);
    } else {
      return hasNoErrors;
    }
  });

  fastify.post('/notifications/:user_id', { schema: { body: postNotifShema } }, async (request, reply) => {

    const userId = request.params.user_id;
    const token = request.headers.authorization;
    const notification = { type: request.body.type, message: request.body.message };


    const hasNoErrors = await sendHttpErrors(token, userId, reply);
    if (hasNoErrors) {

      return await addNotification(userId, notification);
    } else {
      return hasNoErrors;
    }
  });

  fastify.delete('/notifications/:user_id', async (request, reply) => {

    const userId = request.params.user_id;
    const token = request.headers.authorization;

    const hasNoErrors = await sendHttpErrors(token, userId, reply);
    if (hasNoErrors) {
      return await deleteNotifications(userId);
    } else {
      return hasNoErrors;
    }
  });

  fastify.post('/notification/:journey_id', async (request, reply) => {
    const journeyId = request.params.journey_id;
    const token = request.headers.authorization;
    const userId = (await getUserInfos(token)).id;
    const hasNoErrors = await sendHttpErrors(token, userId, reply);
    if (hasNoErrors) {
      return await notifyRegistrationJourney(journeyId, userId);
    } else {
      return hasNoErrors;
    }
  })

}
