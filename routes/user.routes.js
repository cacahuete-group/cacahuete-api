const {
  getAll,
  getOneByEmail,
  getOneByCouple,
  create,
  getOneByCouplePwd,
  connect,
  deleteUser,
  updateUser,
  getById,
} = require('../sequelize/controllers/user.controller');
const {
  deleteReservation,
} = require('../sequelize/controllers/reservation.controller');
const {
  getJourneyById,
} = require('../sequelize/controllers/journey.controller');
const {
  validateToken,
  destroy,
  getUserInfos,
  getAllTokens,
} = require('../sequelize/controllers/token.controller');

const { USER } = require('../sequelize/constants/user.constants');
const { addNotification } = require('../sequelize/controllers/notification.controller');

module.exports = async (fastify, options) => {
  fastify.get('/tokens', async (request, reply) => {
    return getAllTokens();
  });
  // fastify.get('/user/:email', async (request, reply) => {
  //   let user = await getOneByEmail(request.params.email);
  //   if (user) {
  //     return user;
  //   } else {
  //     reply.notFound('User not found');
  //   }
  // }),

  //To get one user
  fastify.get('/users/:userId', async (request, reply) => {
    const token = request.headers.authorization;
    const userId = request.params.userId;

    let response = await getUserInfos(token);

    if (response.id && response.username && response.email && response.type) {
      try {
        const user = await (await getById(userId)).toJSON();
        return {
          email: user.email,
          username: user.username,
          last_name: user.last_name,
          firt_name: user.first_name,
          address: user.address,
          postal_code: user.postal_code,
          city: user.city,
        };
      } catch (e) {
        return reply.notFound(e);
      }
    } else {
      if (response[1] === 'Token not found') reply.notFound(response[1]);
      else reply.unauthorized(response[1]);
    }
  }),
    //Route to create one user
    fastify.post('/users', async (request, reply) => {
      let user = await getOneByCouple(
        request.body.email,
        request.body.username,
      );

      if (user !== null) {
        throw fastify.httpErrors.conflict();
      } else {
        //Avoid passing admin
        request.body.type = 'user';
        const response = await (await create(request.body)).toJSON();
        return { user: response };
      }
    }),
    //Route to connect
    fastify.post('/users/login', async (request, reply) => {
      let user = null;
      try {
        user = await getOneByCouplePwd(
          request.body.email,
          request.body.password,
        );
      } catch (e) {
        reply.notFound('User not found');
      }

      if (user) {
        //Destroy old tokens
        //Delete old tokens
        let tokens = await destroy(user.id);
        let token = await connect(user);
        const {
          id,
          email,
          username,
          type,
          last_name,
          first_name,
          address,
          postal_code,
          city,
        } = user.toJSON();
        return {
          user: {
            id,
            email,
            username,
            type,
            token,
            last_name,
            first_name,
            address,
            postal_code,
            city,
          },
        };
      } else {
        reply.notFound('User not found');
      }
    }),
    //Update a user
    fastify.post('/users/:userId', async (request, reply) => {
      const token = request.headers.authorization;
      const user = request.body.user;
      const userId = request.params.userId;

      let response = await getUserInfos(token);

      if (response.id && response.username && response.email && response.type) {
        if (response.id !== userId && response.type !== USER.ADMIN_TYPE) {
          return reply.unauthorized("User don't have this right");
        }

        const oldUser = await (await getOneByEmail(response.email)).toJSON();

        const userUpdate = await updateUser(user, oldUser);
        const {
          id,
          email,
          username,
          type,
          last_name,
          first_name,
          address,
          postal_code,
          city,
        } = userUpdate.toJSON();
        return {
          user: {
            id,
            email,
            username,
            type,
            token,
            last_name,
            first_name,
            address,
            postal_code,
            city,
          },
        };
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    }),
    // fastify.delete('/user', async (request, reply) => {
    //   const token = request.headers.authorization;
    //   let response = await getUserInfos(token);

    //   if (response.id && response.username && response.email && response.type) {
    //     let userDelete = await deleteUser(response.id);
    //     return userDelete;
    //   } else {
    //     if (response[1] === 'Token not found') reply.notFound(response[1]);
    //     else reply.unauthorized(response[1]);
    //   }
    // }),
    //To delete a user
    fastify.delete('/users/:userId', async (request, reply) => {
      const token = request.headers.authorization;
      const userId = request.params.userId;
      const user = await getById(userId);
      let response = await getUserInfos(token);

      if (response.id && response.username && response.email && response.type) {
        if (!user) return reply.notFound();
        else if (response.id !== userId && response.type !== USER.ADMIN_TYPE) {
          return reply.unauthorized("User don't have this right");
        } else {
          let userDelete = await deleteUser(userId);
          return userDelete;
        }
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    }),
    fastify.get('/users', async (request, reply) => {
      const token = request.headers.authorization;
      let response = await getUserInfos(token);

      if (
        response.id &&
        response.username &&
        response.email &&
        response.type &&
        response.type === USER.ADMIN_TYPE
      ) {
        const users = await getAll();
        return users;
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    });
  //To delete a journey from a user
  fastify.delete(
    '/users/:user_id/journeys/:journey_id',
    async (request, reply) => {
      const token = request.headers.authorization;
      const journeyId = request.params.journey_id;
      const userId = request.params.user_id;

      let response = await getUserInfos(token);

      let journey = null;
      let user = null;
      try {
        journey = await (await getJourneyById(journeyId)).toJSON();
      } catch (e) {
        reply.notFound('Journey id not found');
      }
      try {
        user = await (await getById(userId)).toJSON();
      } catch (e) {
        reply.notFound('User id not found');
      }

      if (response.id && response.username && response.email && response.type) {
        if (response.id !== userId && response.type !== USER.ADMIN_TYPE) {
          return reply.unauthorized("User don't have this right");
        } else {
          let deleteResa = await deleteReservation(response.id, journeyId);
          return deleteResa;
        }
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    },
  );

  fastify.put('/verify', async (request, reply) => {
    let response = await validateToken(request.body.token);
    if (response[0]) {
      return { tokenValid: true };
    } else {
      if (response[1] === 'Token not found') reply.notFound(response[1]);
      else reply.unauthorized(response[1]);
    }
  });
};
