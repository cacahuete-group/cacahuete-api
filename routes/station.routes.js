const {
  getStations,
  stationExists,
  createStation,
  getByID,
} = require('../sequelize/controllers/station.controller');
const {
  validateToken,
  destroy,
} = require('../sequelize/controllers/token.controller');

module.exports = async (fastify, options) => {
  fastify.get('/stations/:id', async (request, reply) => {
    let id = request.params.id;

    const station = await getByID(id);

    if (station !== null) return { station: station };
    else return reply.notFound('Station id not found');
  }),
    fastify.get('/stations', async (request, reply) => {
      if (!request.query.station_name)
        return reply.notImplemented(
          'You should specify a station name in your query parameter',
        );

      let station = await getStations(request.query.station_name);
      return station;

    }),
    fastify.post('/stations', async (request, reply) => {
      const token = request.body.token;
      const station = request.body.station;

      const response = await validateToken(token);

      if (response[0]) {
        const exists = await stationExists(station.id);
        if (!exists) {
          const stationC = await createStation(station);
          return { station: stationC };
        } else {
          return reply.conflict('Station already exists');
        }
      } else {
        if (response[1] === 'Token not found') reply.notFound(response[1]);
        else reply.unauthorized(response[1]);
      }
    });
};
