require('dotenv').config();
const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize(process.env.DB_URL, {
  dialect: 'postgres',
  logging: false,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
  pool: {
    max: 1,
  },
});

const user = require('./models/user.model');
const token = require('./models/token.model');
const station = require('./models/station.model');
const journey = require('./models/journey.model');
const message = require('./models/message.model');
const notification = require('./models/notification.model');
const reservation = require('./models/reservation.model');

const links = require('./links');

user(sequelize, DataTypes);
token(sequelize, DataTypes);
station(sequelize, DataTypes);
journey(sequelize, DataTypes);
message(sequelize, DataTypes);
notification(sequelize, DataTypes);
reservation(sequelize, DataTypes);

links(sequelize);

sequelize.sync();
module.exports = sequelize;
