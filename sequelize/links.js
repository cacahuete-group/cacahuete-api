//Make links between Models
module.exports = (sequelize) => {
  const { User, Message, Journey, Station, Token, Reservation, Notification } =
    sequelize.models;

  User.hasMany(Message, { onDelete: 'CASCADE' });
  Message.belongsTo(User, { foreignKey: 'sender' }, { onDelete: 'CASCADE' });
  Message.belongsTo(User, { foreignKey: 'receiver' }, { onDelete: 'CASCADE' });

  Journey.belongsTo(
    Station,
    {
      as: 'departureStation',
      foreignKey: 'departure_station',
    },
    { onDelete: 'CASCADE' },
  );
  Journey.belongsTo(
    Station,
    {
      as: 'arrivalStation',
      foreignKey: 'arrival_station',
    },
    { onDelete: 'CASCADE' },
  );
  Station.hasMany(Journey, { as: 'departureStation' }, { onDelete: 'CASCADE' });

  Journey.belongsToMany(
    User,
    { through: Reservation },
    { onDelete: 'CASCADE' },
  );
  User.belongsToMany(Journey, { through: Reservation }),
    { onDelete: 'CASCADE' };

  User.hasMany(Token, { onDelete: 'CASCADE' });
  Token.belongsTo(User, { onDelete: 'CASCADE' });

  User.hasMany(Token, { onDelete: 'CASCADE' });
  Notification.belongsTo(User, { onDelete: 'CASCADE' });
};
