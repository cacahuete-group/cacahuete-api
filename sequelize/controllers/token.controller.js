const { models } = require('..');
const { Op } = require('sequelize');
const jwt = require('jsonwebtoken');
const { USER } = require('../constants/user.constants');
require('dotenv').config();

const sendHisToken = async (token, userId) => {
  const user = await getUserInfos(token);

  if (user.id && user.username && user.email && user.type) {
    return userId === user.id;
  } else {
    return false;
  }
}

const hasAdminToken = async (token, userId) => {
  const user = await getUserInfos(token);

  if (user.id && user.username && user.email && user.type) {
    return userId === user.id && user.type === USER.ADMIN_TYPE;
  } else {
    return false;
  }
}

const sendHttpErrors = async (token, userId = false, reply, adminCanPerformOther = false) => {

  let response = await getUserInfos(token);
  if (!userId) {
    userId = response.id;
  }
  if (response.id && response.username && response.email && response.type) {
    if (!(response.id === userId || adminCanPerformOther && response.type === USER.ADMIN_TYPE)) {
      return reply.unauthorized("User don't have this right");
    }
    return true;
  } else {
    if (response[1] === 'Token not found') return reply.notFound(response[1]);
    else return reply.unauthorized(response[1]);
  }

}


function generateToken(id, email, username, type) {
  userObj = {
    id: JSON.stringify(id),
    username: username,
    email: email,
    type: type,
  };
  const token = jwt.sign(userObj, process.env.DB_SECRET, {
    expiresIn: '30d',
  });
  return token;
}

function parseToken(tokenvalue) {
  token = jwt.verify(tokenvalue, process.env.DB_SECRET);
  //token.id = parseInt(token.id);
  return token;
}

async function getUserInfos(tokenvalue) {
  const response = await validateToken(tokenvalue);
  if (response[0]) {
    return parseToken(tokenvalue);
  } else return response;
}

async function validateToken(tokenvalue) {
  //Check if the token is valid and not expired
  // Return false and error message
  try {
    token = jwt.verify(tokenvalue, process.env.DB_SECRET);
  } catch (e) {
    return [false, 'Token ' + e.message.replace('jwt ', '')];
  }

  //Check if is presents in DB
  let tokenDB = await models.Token.findOne({
    where: {
      value: tokenvalue,
    },
  });

  if (tokenDB !== null) {
    return [true, 'x-token'];
  } else {
    return [false, 'Token not found'];
  }
}

async function createToken(user_id, email, username, type) {
  const token = generateToken(user_id, email, username, type);
  let tokenCreated = await models.Token.create({
    value: token,
    UserId: user_id,
  });
  return tokenCreated;
}

async function destroy(user_id) {
  let tokens = await models.Token.destroy({
    where: {
      UserId: user_id,
    },
  });
  return tokens;
}

async function getAllTokens() {
  return await models.Token.findAll({});
}

module.exports = {
  createToken,
  validateToken,
  parseToken,
  destroy,
  getUserInfos,
  getAllTokens,
  sendHttpErrors,
  generateToken
};
