const { models } = require('..');
const { stationExists, createStation } = require('./station.controller');

const { validateToken, getUserInfos } = require('./token.controller');
const { Op } = require('sequelize');

const deleteJourney = async (id) => {
  let journey = await models.Journey.destroy({ where: { id: id } });
  return journey;
};

const countFromJourney = async (idJourney) => {
  const nbReservation = await models.Reservation.count({
    where: {
      JourneyId: idJourney,
    },
  });
  return nbReservation;
};

const getFromUser = async (id) => {
  //id = user.id ?? (await getUserInfos(user.token).id) ?? null;

  let journeys = await models.User.findOne({
    where: { id: id },

    include: [
      {
        model: models.Journey,

        include: [
          {
            model: models.Station,
            as: 'departureStation',
          },
          {
            model: models.Station,
            as: 'arrivalStation',
          },
        ],
      },
    ],
  });
  const processJourneys = await journeys.toJSON().Journeys.map(async (e) => {
    const userCount = await countFromJourney(e.id);
    return { ...e, userCount: userCount };
  });
  journeys = await Promise.all(processJourneys);
  return { journeys: journeys };
};

const getUsersIdJourneys = async (idJourney) => {
  const { count, rows } = await models.Reservation.findAndCountAll({
    attributes: ["UserId"],
    where: {
      JourneyId: idJourney
    }
  });

  return { users: rows, count: count };
}

const getJourneys = async (page, per_page) => {
  const { count, rows } = await models.Journey.findAndCountAll({
    include: [
      {
        model: models.Station,
        as: 'departureStation',
      },
      {
        model: models.Station,
        as: 'arrivalStation',
      },
    ],
    order: [['departure_date', 'DESC']],
    offset: (page - 1) * per_page, //Start at zero but take 1 as param
    limit: per_page,
  });
  const processJourneys = await rows.map(async (e) => {
    const userCount = await countFromJourney(e.id);
    return { ...e.toJSON(), userCount: userCount };
  });
  journeys = await Promise.all(processJourneys);
  return { journeys: journeys, count: count };
};
//https://sequelize.org/v7/manual/model-querying-finders.html
//findAndCOunt All for nearby places
//[Op.iLike]
//TODO for queries near address user

const getJourney = async (journey) => {
  const journeyQ = await models.Journey.findOne({
    where: {
      departure_date: journey.departure_date,
      arrival_date: journey.arrival_date,
      departure_station: journey.departure_station.id,
      arrival_station: journey.arrival_station.id,
      id_train: journey.id_train,
    },
  });
  return journeyQ;
};

const searchJourneyAround = async (city, postal_code) => {
  searchQueryCity = null;
  searchQueryCP = null;
  searchQuery = {};

  if (postal_code) {
    postal_code = postal_code.slice(0, 2);
    rangeMin = parseInt(postal_code + '000');
    rangeMax = parseInt(postal_code + '999');
    searchQueryCP = {
      postal_code: {
        [Op.and]: [
          {
            [Op.gte]: rangeMin,
            [Op.lte]: rangeMax,
          },
        ],
      },
    };
    searchQuery = searchQueryCP;
  }
  if (city) {
    searchQueryCity = {
      city: {
        [Op.like]: `%${city}%`,
      },
    };
    searchQuery = searchQueryCity;
  }

  if (postal_code && city) {
    searchQuery = {
      [Op.or]: [searchQueryCP, searchQueryCity],
    };
  }

  const { count, rows } = await models.Journey.findAndCountAll({
    include: [
      {
        model: models.Station,
        as: 'departureStation',
        where: searchQuery,
      },
      {
        model: models.Station,
        as: 'arrivalStation',
      },
    ],
  });
  return { journeys: rows, count: count };
};

const getJourneyStation = async (journeyId) => {
  const journey = await models.Journey.findOne({
    include: [
      {
        model: models.Station,
        as: 'departureStation',
      },
      {
        model: models.Station,
        as: 'arrivalStation',
      },
    ],
    where: { id: journeyId },
  });
  return journey;
};

const existsJourney = async (journey) => {
  const journeyE = await get(journey);
  return journeyE !== null;
};

const getJourneyById = async (id) => {
  return await models.Journey.findOne({ where: { id: id } });
};

//Create a journey, bind it to a user and create station if they don't exists
const createJourney = async (journey, departureStation, arrivalStation, id) => {
  if (journey?.id ?? false) {
    await models.Reservation.create({
      JourneyId: journey.id,
      UserId: id,
    });
    return journey;
  }

  const departureStationExists = await stationExists(departureStation.id);
  const arrivalStationExists = await stationExists(arrivalStation.id);

  if (!departureStationExists) {
    await createStation(departureStation);
  }
  if (!arrivalStationExists) {
    await createStation(arrivalStation);
  }

  const journeyQ = (await getJourney(journey))?.toJSON();

  const existsJ = journeyQ !== undefined;

  let journeyI = null;


  //Only appends if does'nt exists in DB
  if (!existsJ) {
    journeyI = await models.Journey.create({
      departure_date: journey.departure_date,
      arrival_date: journey.arrival_date,
      id_train: journey.id_train,
      arrival_station: journey.arrival_station.id,
      departure_station: journey.departure_station.id,
    });
  }

  //Make the link between Journey and user only if it doesn't have one already
  const journeyId = journeyQ?.id ?? journeyI.toJSON().id;

  const reservationQ = await models.Reservation.findOne({
    where: {
      JourneyId: journeyId,
      UserId: id,
    },
  });

  if (!reservationQ) {
    await models.Reservation.create({
      JourneyId: journeyId,
      UserId: id,
    });
  } else {
    return null;
  }

  //Return journey
  let journeyReturn = null;
  if (!existsJ) journeyReturn = journeyI;
  else journeyReturn = journeyQ;

  return await getJourneyStation(journeyReturn.id);
};

module.exports = {
  createJourney,
  existsJourney,
  getJourney,
  getJourneyById,
  getFromUser,
  deleteJourney,
  countFromJourney,
  getJourneys,
  getJourneyStation,
  searchJourneyAround,
  getUsersIdJourneys
};
