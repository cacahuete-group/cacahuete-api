const axios = require('axios');
const { models } = require('..');

const getStations = async (beginCityName) => {
  //To get all infos on stations
  let response = await axios.get(
    `
    https://api.sncf.com/v1/coverage/sncf/places?q=${beginCityName}&type%5B%5D=stop_area&`,
    { headers: { Authorization: `${process.env.API_KEY}` } },
  );
  if (!response.data?.places ?? true) return null;

  stations = response.data.places.map((e) => {
    const station_name = e.name;
    const id = parseInt(e.id.split`:`[2]);
    const region = e.stop_area.administrative_regions[0];
    const insee = region.insee ?? 0;
    const city = region.name;
    const postal_code =
      region.zip_code.length > 0 ? region.zip_code.split`;`[0] : insee;

    return {
      id: id,
      station_name: station_name,
      city: city,
      postal_code: postal_code,
      insee: insee,
    };
  });

  return { stations: stations };
};

const getStations2 = async (beginCityName) => {
  let response = await axios.get(
    `https://${process.env.API_KEY}@api.navitia.io/v1/coverage/sncf/places/?q=${beginCityName}`,
  );

  stations = response.data.places.map((e) => {
    const region = e.administrative_region;
    const id = region.insee;
    const city = region.name;
    const postal_code = region.zip_code.split`;`[0];
    return { station: { id: id, city: city, postal_code: postal_code } };
  });

  return stations;
};

const getByID = async (id) => {
  let station = await models.Station.findOne({
    where: {
      id: id,
    },
  });
  return station;
};

const stationExists = async (id) => {
  let station = await getByID(id);
  return station !== null;
};

const createStation = async (station) => {
  let stationI = await models.Station.create(station);
  return stationI;
};

module.exports = {
  getByID,
  getStations,
  stationExists,
  createStation,
};
