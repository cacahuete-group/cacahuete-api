const { models } = require('..');
const { Op } = require('sequelize');
const { getUsersIdJourneys } = require('./journey.controller');
const { NOTIF_TYPE } = require('../constants/notification.constants');


const getNotifications = async (idUser) => {
  const { count, rows } = await models.Notification.findAndCountAll({
    where: { UserId: idUser }
  });

  return { notifications: rows, count: count };
}

const addNotification = async (idUser, notification) => {

  return await models.Notification.create({
    message: notification.message,
    type: notification.type,
    UserId: idUser,
  });
}

const notifyRegistrationJourney = async (idJourney, idUser) => {
  const { users, count } = await getUsersIdJourneys(idJourney);

  notifications = users.filter(user => user.UserId != idUser)
    .map(user => ({
      UserId: user.UserId,
      type: NOTIF_TYPE.REGISTER_JOURNEY,
      message: "Another user registered to one of your journey."
    }))//.filter(user => user.UserId !== idUser);
  return await models.Notification.bulkCreate(notifications);

}

const deleteNotifications = async (idUser) => {
  return await models.Notification.destroy({ where: { UserId: idUser } });
}

module.exports = {
  getNotifications,
  addNotification,
  deleteNotifications,
  notifyRegistrationJourney
};