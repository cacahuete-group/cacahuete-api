const { models } = require('../');
const { Op } = require('sequelize');
const bcrypt = require('bcrypt');
const { createToken } = require('./token.controller');
const { deleteReservations } = require('./reservation.controller');

require('dotenv').config();

async function getAll() {
  let { rows, count } = await models.User.findAndCountAll();
  return { users: rows, count: count };
}

async function getByEmail(email) {
  let user = await models.User.findOne({
    where: {
      email: email,
    },
  });
  return user;
}

async function getById(id) {
  let user = await models.User.findOne({
    where: {
      id: id,
    },
  });
  user; //.toJSON();//.filter((e) => e !== 'password' || e !== 'type');
  return user;
}

function isUserOk(user) {
  let isOk = true;
  return isOk;
}

async function getOneByCouple(email, username) {
  let user = await models.User.findOne({
    where: {
      [Op.or]: [
        {
          email: email,
        },
        {
          username: username,
        },
      ],
    },
  });
  return user;
}

async function getOneByCouplePwd(email, password) {
  let user = await getOneByEmail(email);
  let areSame = await arePasswordEquals(password, user.password);
  if (!areSame) {
    return null;
  }

  return user;
}

async function getOneByEmail(email) {
  let exists = await userExists(email);
  if (exists) {
    let user = await getByEmail(email);
    return user;
  } else {
    return null;
  }
}

async function userExists(email) {
  let user = await getByEmail(email);
  return user !== null;
}

async function hashPassword(password) {
  let hashed = await bcrypt.hash(password, 10);
  return hashed;
}

async function arePasswordEquals(p1, p2) {
  let areSame = await bcrypt.compare(p1, p2);
  return areSame;
}

async function create(user) {

  //Hash password
  user.password = await hashPassword(user.password);

  let userI = await models.User.create(user);

  return userI;
}

async function deleteUser(userId) {
  const deletedReservation = await deleteReservations(userId);

  const deletedUser = await models.User.destroy({
    where: {
      id: userId,
    },
  });
  return deletedUser;
}

async function updateUser(user, oldUser) {
  if (user?.password) user.password = await hashPassword(user.password);
  let updatedUser = {
    username: user.username,
    last_name: user.last_name,
    first_name: user.first_name,
    password: user?.password ?? oldUser.password,
    address: user?.address ?? oldUser.addresss,
    city: user?.city ?? oldUser.city,
  };
  if (user?.postal_code) {
    updatedUser.postal_code = user.postal_code;
  } else {
    updatedUser.postal_code = null;
  }
  await models.User.update(updatedUser, {
    where: {
      id: oldUser.id,
    },
  });

  return await getByEmail(oldUser.email);
}

async function connect(user) {
  let token = await createToken(user.id, user.email, user.username, user.type);
  return token.value;
}

module.exports = {
  getAll,
  getOneByEmail,
  getOneByCouple,
  create,
  isUserOk,
  connect,
  getOneByCouplePwd,
  connect,
  updateUser,
  deleteUser,
  getById,
};
