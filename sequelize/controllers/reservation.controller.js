const { models } = require('..');
const {
  deleteJourney,
  countFromJourney,
  getJourneyStation,
} = require('./journey.controller');

const getCountFromJourney = async (idJourney) => {

  const nbReservation = await models.Reservation.count({
    where: {
      JourneyId: idJourney,
    },
  });
  return nbReservation;
};

const deleteReservations = async (UserId) => {
  try {
    const reservation = await models.Reservation.destroy({
      UserId: UserId,
    });
    return reservation;
  } catch (error) {
    return null;
  }
};

const createReservation = async (UserId, JourneyId) => {
  try {
    const reservation = await models.Reservation.create({
      UserId: UserId,
      JourneyId: JourneyId,
    });
    let journey = await (await getJourneyStation(JourneyId)).toJSON();
    journey.userCount = await getCountFromJourney(JourneyId);
    return journey;
  } catch (error) {
    return null;
  }
};

const deleteReservation = async (UserId, JourneyId) => {
  const countJourney = await countFromJourney(JourneyId);

  const deletedResa = await models.Reservation.destroy({
    where: {
      UserId: UserId,
      JourneyId: JourneyId,
    },
  });

  if (countJourney === 1) {
    await deleteJourney(JourneyId);
  }

  return deletedResa;
};

module.exports = {
  getCountFromJourney,
  deleteReservation,
  createReservation,
  deleteReservations,
};
