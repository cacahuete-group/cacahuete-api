
const NOTIF_TYPE = {
  REGISTER_JOURNEY: 'Register journey',
  DELETE_JOURNEY: 'Delete journey'
}


const NOTIF_ENUM = Object.values(NOTIF_TYPE)


const postNotifShema = {
  type: 'object',
  required: ['type', 'message'],
  properties: {
    type: { type: 'string', enum: NOTIF_ENUM },
    message: { type: 'string' }
  },
};

module.exports = {
  postNotifShema,
  NOTIF_TYPE
}