const USER = {
  ADMIN_TYPE: 'admin',
  USER_TYPE: 'user',
};

module.exports = {
  USER,
};
