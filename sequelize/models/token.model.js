module.exports = (sequelize, DataTypes) => {
  sequelize.define('Token', {
    value: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    expirationDate: {
      allowNull: true,
      type: DataTypes.DATE,
    },
  });
};
