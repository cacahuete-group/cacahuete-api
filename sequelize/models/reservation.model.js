module.exports = (sequelize, DataTypes) => {
  sequelize.define('Reservation', {
    UserId: {
      type: DataTypes.INTEGER,
      references: {
        model: sequelize.models.User,
        key: 'id',
      },
    },
    JourneyId: {
      type: DataTypes.INTEGER,
      references: {
        model: sequelize.models.Journey,
        key: 'id',
      },
    },
  });
};
