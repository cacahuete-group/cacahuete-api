module.exports = (sequelize, DataTypes) => {
  sequelize.define('Journey', {
    departure_date: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    arrival_date: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    id_train: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
  });
};
