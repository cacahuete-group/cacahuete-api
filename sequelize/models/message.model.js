module.exports = (sequelize, DataTypes) => {
  sequelize.define('Message', {
    message: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    read_time: {
      allowNull: true,
      default: null,
      type: DataTypes.DATE,
    },
  });
};
