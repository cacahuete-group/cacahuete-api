module.exports = (sequelize, DataTypes) => {

  sequelize.define('Notification', {
    message: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    type: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
  });
};

