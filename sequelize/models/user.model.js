const { USER } = require('../constants/user.constants');

module.exports = (sequelize, DataTypes) => {
  sequelize.define('User', {
    email: {
      allowNull: false,
      unique: true,
      // primaryKey: true,
      type: DataTypes.STRING(40),
    },
    username: {
      allowNull: false,
      type: DataTypes.STRING(20),
      unique: true,
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING(64),
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM(USER.ADMIN_TYPE, USER.USER_TYPE),
      default: USER.USER_TYPE,
    },
    first_name: {
      allowNull: false,
      type: DataTypes.STRING(30),
    },
    last_name: {
      allowNull: false,
      type: DataTypes.STRING(30),
    },
    address: {
      allowNull: true,
      type: DataTypes.STRING(50),
    },
    postal_code: {
      allowNull: true,
      type: DataTypes.INTEGER,
    },
    city: {
      allowNull: true,
      type: DataTypes.STRING(50),
    },
  });
};
