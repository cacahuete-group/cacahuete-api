module.exports = (sequelize, DataTypes) => {
  sequelize.define('Station', {
    id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    station_name: {
      allowNull: false,
      type: DataTypes.STRING(50),
    },
    city: {
      allowNull: false,
      type: DataTypes.STRING(50),
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    insee: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
  });
};
